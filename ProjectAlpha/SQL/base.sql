﻿Create Database Project_Alpha
GO
Use Project_Alpha
GO

/*Delete tables before creating*/
/* Table: dbo.User */

/* Table: dbo.Rental */
if exists (select * from sysobjects 
  where id = object_id('dbo.Rental') and sysstat & 0xf = 3)
  drop table dbo.Rental
GO
/* Table: dbo.Feedback */
if exists (select * from sysobjects 
  where id = object_id('dbo.Feedback') and sysstat & 0xf = 3)
  drop table dbo.Feedback
GO
/* Table: dbo.Game */
if exists (select * from sysobjects 
  where id = object_id('dbo.Game') and sysstat & 0xf = 3)
  drop table [dbo].[Game]
GO
if exists (select * from sysobjects 
  where id = object_id('dbo.User') and sysstat & 0xf = 3)
  drop table [dbo].[User]
GO
/* Table: dbo.Log */
if exists (select * from sysobjects
	where id = object_id('dbo.Log') and sysstat & 0xf = 3)
	drop table [dbo].[Log]
GO

/*Create tables*/
/* Table: dbo.User */
CREATE TABLE [dbo].[User]
(
  UserID				int IDENTITY (1,1),
  EmailAddr				varchar(99)				NOT NULL,
  Name					varchar(99)				NOT NULL,
  Password				varchar(99)				NOT NULL,      
  CONSTRAINT PK_User PRIMARY KEY NONCLUSTERED (UserID),
)
GO
/* Table: dbo.Game */
CREATE TABLE dbo.Game
(
  GameID				int IDENTITY (1,1),
  Title					varchar(99)				NOT NULL,
  Description			varchar(3000)			NOT NULL,
  Age					int						NOT NULL,
  Category				varchar(99)				NOT NULL,
  ReleaseDate			datetime				NOT NULL DEFAULT getdate(),   
  Quantity				int						NULL,  

  CONSTRAINT PK_Game PRIMARY KEY NONCLUSTERED (GameID),
)

/* Table: dbo.User */
CREATE TABLE dbo.Feedback
(
  FeedbackID				int IDENTITY (1,1),
  GameID					int					NOT NULL,
  UserID					int					NOT NULL,
  Rating					decimal				NOT NULL,
  Feedback					varchar(3000)			NOT NULL,      
  CONSTRAINT PK_Feedback PRIMARY KEY NONCLUSTERED (FeedbackID),
  CONSTRAINT FK_Feedback_UserID FOREIGN KEY (UserID) REFERENCES [dbo].[User](UserID),
  CONSTRAINT FK_Feedback_GameID FOREIGN KEY (GameID) REFERENCES [dbo].[Game](GameID)
)
GO

GO
/* Table: dbo.Rental */
CREATE TABLE dbo.Rental
(
  RentalID				int IDENTITY (1,1),
  GameID				int						NOT NULL,
  RentalDuration		int						NOT NULL,
  StartDate				datetime				NOT NULL DEFAULT getdate(),   
  EndDate				datetime				NOT NULL,  
  RentedTo				int						NOT NULL,
  RentalPrice			money					NOT NULL,
  CreditCardNo			varchar(99)				NOT NULL,
  Status				char(1)					NOT NULL,
  CONSTRAINT PK_Rental PRIMARY KEY NONCLUSTERED (RentalID),
  CONSTRAINT FK_Rental_RentedTo FOREIGN KEY (RentedTo) REFERENCES [dbo].[User](UserID),
  CONSTRAINT FK_Rental_GameID FOREIGN KEY (GameID) REFERENCES [dbo].[Game](GameID)
)
GO
/* Table: dbo.Log */
CREATE TABLE [dbo].[Log]
(
  LogID				int IDENTITY (1,1),
  Description		varchar(3000) NOT NULL
  CONSTRAINT PK_Log PRIMARY KEY (LogID)
)
GO

/*Sample Data*/
SET IDENTITY_INSERT [dbo].[User] ON
INSERT [dbo].[User] ([UserID],[EmailAddr], [Name], [Password]) VALUES (1,'gg@email.com' ,'Brian Chew', 'password')
INSERT [dbo].[User] ([UserID],[EmailAddr], [Name], [Password]) VALUES (2,'gg@email.com.sg', 'Loh Zong Yuan', 'password')
SET IDENTITY_INSERT [dbo].[User] OFF
SELECT * FROM dbo.[User]
SET IDENTITY_INSERT [dbo].[Game] ON
INSERT [dbo].[Game] ([GameID], [Title], [Description], [Age], [Category], [ReleaseDate], [Quantity]) VALUES (1, 'Playerunknown Battlegrounds', 'This is just a placeholder', 16, 'FPS', '18-July-2017', 10)
INSERT [dbo].[Game] ([GameID], [Title], [Description], [Age], [Category], [ReleaseDate], [Quantity]) VALUES (2, 'zONG YUAN BATTLEGROUND', 'This is just a placeholder', 16, 'FPS', '18-July-2017', 10)

SET IDENTITY_INSERT [dbo].[Game] OFF
SELECT * FROM Game
SET IDENTITY_INSERT [dbo].[Rental] ON
INSERT [dbo].[Rental] ([RentalID], [GameID], [RentalDuration], [StartDate], [EndDate], [RentedTo], [RentalPrice], [CreditCardNo], [Status]) VALUES (1, 1, 2, '18-July-2018', '23-July-2018', 2, 1, '4600-0000-0000-0000', 'A')
SET IDENTITY_INSERT [dbo].[Rental] OFF
SELECT * FROM Rental
SELECT * FROM [dbo].[Feedback]
SET IDENTITY_INSERT [dbo].[Log] ON
INSERT [dbo].[Log] ([LogID], [Description]) VALUES (1, 'Test')
SET IDENTITY_INSERT [dbo].[Log] OFF
SELECT * FROM [dbo].[Log]